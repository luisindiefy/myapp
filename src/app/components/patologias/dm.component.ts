import { Component, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-dm',
  templateUrl: 'dm.component.html',
  
  
  
})
export class DmComponent {
public name = 'Diabetes Mellitus';
public signos = ['lesion o pie diabético', 'problemas de cognición', 'hiper o hipoglucemia', 'inclumpimiento del tratamiento'];
 
@ViewChild('dmsig') ul: ElementRef;

mostrar(){
  document.getElementById('dmsig').style.display = 'block';
}
ocultar(){
  document.getElementById('dmsig').style.display = 'none';
}
mosocu(){
  var sig = document.getElementById('dmsig');
  if( sig.style.display == 'none'){
    this.mostrar()
  } else {
    this.ocultar()
  }
}
}