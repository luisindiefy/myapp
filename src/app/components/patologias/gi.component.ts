import { Component, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-gi',
  templateUrl: 'gi.component.html',
  
  
  
})
export class GiComponent {
public name = 'GastroEnteritis';
public signos = ['Diarrea', 'Fiebre', 'Dolor agudo'];
 
@ViewChild('gisig') ul: ElementRef;

mostrar(){
  document.getElementById('gisig').style.display = 'block';
}
ocultar(){
  document.getElementById('gisig').style.display = 'none';
}
mosocugi(){
  let sig = document.getElementById('gisig');
  if( sig.style.display == 'none'){
    this.mostrar()
  } else {
    this.ocultar()
  }
}
}