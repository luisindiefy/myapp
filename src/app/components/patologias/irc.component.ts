import { Component, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-irc',
  templateUrl: 'irc.component.html',
  
  
  
})
export class IrcComponent {
public name = 'Insuficiencia Renal Crónica';
public signos = ['Edema', 'Desnutrición', 'Desapego al tratamiento'];
 
@ViewChild('ircsig') ul: ElementRef;

mostrar(){
  document.getElementById('ircsig').style.display = 'block';
}
ocultar(){
  document.getElementById('ircsig').style.display = 'none';
}
mosocuirc(){
  let sig = document.getElementById('ircsig');
  if( sig.style.display == 'none'){
    this.mostrar()
  } else {
    this.ocultar()
  }
}
}