import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import {DmComponent } from './components/patologias/dm.component'
import {GiComponent } from './components/patologias/gi.component'
import {IrcComponent } from './components/patologias/irc.component'



@NgModule({
  declarations: [
    AppComponent,
    DmComponent, GiComponent, IrcComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
